"""
I am module. I contain functions, classes.
I am usually not executable and I am used
as a source for other scripts or modules.
"""

def greet(name):
    print('Hello {}!'.format(name))
