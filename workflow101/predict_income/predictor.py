from celery import Celery

app = Celery('tasks', broker='redis://redis:6379/0', backend='redis://redis:6379/0', queue='income')

@app.task
def predict(input_value):
    return input_value + 100

@app.task
def retrain():
    return 'Model retrained.'
